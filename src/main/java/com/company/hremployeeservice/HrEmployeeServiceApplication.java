package com.company.hremployeeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrEmployeeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(HrEmployeeServiceApplication.class, args);
    }
}
