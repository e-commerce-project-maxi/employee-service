package com.company.hremployeeservice.checker;

import com.company.hremployeeservice.myException.BusinessExceptionEnum;
import com.company.hremployeeservice.myException.ConflictException;
import com.company.hremployeeservice.service.department.DepartmentBusinessService;
import com.company.hremployeeservice.service.department.DepartmentFunctionalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class DepartmentRepetitionChecker {

    private final DepartmentFunctionalService service;

    public void check(String name) {
        log.info("Check if Department has already registered with particular name: {}", name);
        if (service.existsByName(name)) {
            throw new ConflictException(BusinessExceptionEnum.DEPARTMENT_IS_ALREADY_EXISTS, name);
        }
    }
}
