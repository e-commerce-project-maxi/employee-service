package com.company.hremployeeservice.controller;

import com.company.hremployeeservice.dto.request.AddressRequest;
import com.company.hremployeeservice.service.address.AddressBusinessService;
import com.company.hremployeeservice.service.address.AddressFunctionalService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressBusinessService service;

    @PostMapping
    public ResponseEntity<?> create(@RequestBody AddressRequest request) {
        return ResponseEntity.ok(service.create(request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(service.findById(id));
    }
}
