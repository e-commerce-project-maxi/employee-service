package com.company.hremployeeservice.dto.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressRequest {

    String addressLine1;
    String addressLine2;
    String country;
    String city;
}
