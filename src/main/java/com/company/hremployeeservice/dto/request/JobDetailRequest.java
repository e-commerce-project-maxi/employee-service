package com.company.hremployeeservice.dto.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JobDetailRequest {

    LocalDate startDate;
    LocalDate endDate;
    BigDecimal currentSalary;
    BigDecimal lastSalary;
}
