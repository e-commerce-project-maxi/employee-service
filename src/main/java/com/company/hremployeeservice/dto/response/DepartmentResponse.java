package com.company.hremployeeservice.dto.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DepartmentResponse {

    Long id;
    String departmentName;
    LocalDateTime createdAt;
    LocalDateTime modifiedAt;
    String departmentDirector;
}
