package com.company.hremployeeservice.mapper;

import com.company.hremployeeservice.domain.Address;
import com.company.hremployeeservice.dto.request.AddressRequest;
import com.company.hremployeeservice.dto.response.AddressResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    Address toEntity(AddressRequest request);

    AddressResponse toResponse(Address address);
}
