package com.company.hremployeeservice.mapper;

import com.company.hremployeeservice.domain.Department;
import com.company.hremployeeservice.dto.request.DepartmentRequest;
import com.company.hremployeeservice.dto.response.DepartmentResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DepartmentMapper {

    Department toEntity(DepartmentRequest request);

    DepartmentResponse toResponse(Department department);
}
