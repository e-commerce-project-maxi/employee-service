package com.company.hremployeeservice.mapper;

import com.company.hremployeeservice.domain.Employee;
import com.company.hremployeeservice.dto.request.EmployeeRequest;
import com.company.hremployeeservice.dto.response.EmployeeResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

    Employee toEntity(EmployeeRequest request);

    EmployeeResponse toResponse(Employee employee);
}
