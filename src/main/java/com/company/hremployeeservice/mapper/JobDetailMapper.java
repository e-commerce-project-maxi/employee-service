package com.company.hremployeeservice.mapper;

import com.company.hremployeeservice.domain.JobDetail;
import com.company.hremployeeservice.dto.request.JobDetailRequest;
import com.company.hremployeeservice.dto.response.JobDetailResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface JobDetailMapper {

    JobDetail toEntity(JobDetailRequest request);

    JobDetailResponse toResponse(JobDetail jobDetail);
}
