package com.company.hremployeeservice.myException;

import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Getter
public class BaseException extends RuntimeException {

    String msg;
    String code;
    String description;
}
