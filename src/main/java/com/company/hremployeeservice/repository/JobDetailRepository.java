package com.company.hremployeeservice.repository;

import com.company.hremployeeservice.domain.JobDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobDetailRepository extends JpaRepository<JobDetail, Long> {
}
