package com.company.hremployeeservice.service.address;

import com.company.hremployeeservice.dto.request.AddressRequest;
import com.company.hremployeeservice.myException.common.CommonResponse;

public interface AddressBusinessService {

    CommonResponse create(AddressRequest request);

    CommonResponse findById(Long id);

    CommonResponse findAll();

    CommonResponse update(Long id, AddressRequest request);
}
