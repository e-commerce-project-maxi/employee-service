package com.company.hremployeeservice.service.address;

import com.company.hremployeeservice.domain.Address;
import com.company.hremployeeservice.dto.request.AddressRequest;
import com.company.hremployeeservice.mapper.AddressMapper;
import com.company.hremployeeservice.myException.common.CommonResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressBusinessServiceImpl implements AddressBusinessService {

    private final AddressFunctionalService service;
    private final AddressMapper mapper;

    @Override
    public CommonResponse create(AddressRequest request) {
        CommonResponse response = new CommonResponse();
        Address address = mapper.toEntity(request);
        response.setItem(mapper.toResponse(service.create(address)));
        return response;
    }

    @Override
    public CommonResponse findById(Long id) {
        CommonResponse response = new CommonResponse();
        response.setItem(mapper.toResponse(service.findById(id)));
        return response;
    }

    @Override
    public CommonResponse findAll() {
        CommonResponse response = new CommonResponse();
        response.setItem(service.findAll().stream().map(mapper::toResponse).collect(Collectors.toList()));
        return response;
    }

    @Override
    public CommonResponse update(Long id, AddressRequest request) {
        CommonResponse response = new CommonResponse();
        Address address = mapper.toEntity(request);
        response.setItem(mapper.toResponse(service.update(id, address)));
        return response;
    }
}
