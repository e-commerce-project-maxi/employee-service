package com.company.hremployeeservice.service.address;

import com.company.hremployeeservice.domain.Address;
import com.company.hremployeeservice.dto.request.AddressRequest;

import java.util.List;

public interface AddressFunctionalService {

    Address create(Address address);

    Address findById(Long id);

    List<Address> findAll();

    Address update(Long id, Address address);
}
