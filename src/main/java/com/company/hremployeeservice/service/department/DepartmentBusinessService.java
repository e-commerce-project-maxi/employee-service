package com.company.hremployeeservice.service.department;

import com.company.hremployeeservice.dto.request.DepartmentRequest;
import com.company.hremployeeservice.myException.common.CommonResponse;

public interface DepartmentBusinessService {

    CommonResponse create(DepartmentRequest request);

    CommonResponse findById(Long id);

    CommonResponse findAll();

    CommonResponse update(Long id, DepartmentRequest request);
}
