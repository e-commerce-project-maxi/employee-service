package com.company.hremployeeservice.service.department;

import com.company.hremployeeservice.domain.Department;
import com.company.hremployeeservice.dto.request.DepartmentRequest;
import com.company.hremployeeservice.myException.common.CommonResponse;

import java.util.List;

public interface DepartmentFunctionalService {

    Department create(Department department);

    Department findById(Long id);

    List<Department> findAll();

    Department update(Long id, Department department);

    boolean existsByName(String name);
}
