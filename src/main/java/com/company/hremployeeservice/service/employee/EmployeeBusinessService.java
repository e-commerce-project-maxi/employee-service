package com.company.hremployeeservice.service.employee;

import com.company.hremployeeservice.dto.request.EmployeeRequest;
import com.company.hremployeeservice.myException.common.CommonResponse;

public interface EmployeeBusinessService {

    CommonResponse create(EmployeeRequest request);

    CommonResponse findById(Long id);

    CommonResponse findAll();

    CommonResponse update(Long id, EmployeeRequest request);

    CommonResponse delete(Long id);
}
