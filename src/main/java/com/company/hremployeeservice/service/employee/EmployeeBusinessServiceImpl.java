package com.company.hremployeeservice.service.employee;

import com.company.hremployeeservice.checker.DepartmentRepetitionChecker;
import com.company.hremployeeservice.checker.EmailAvailabilityChecker;
import com.company.hremployeeservice.checker.PhoneNumberAvailabilityChecker;
import com.company.hremployeeservice.domain.Employee;
import com.company.hremployeeservice.dto.request.EmployeeRequest;
import com.company.hremployeeservice.dto.response.EmployeeResponse;
import com.company.hremployeeservice.mapper.EmployeeMapper;
import com.company.hremployeeservice.myException.common.CommonResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeBusinessServiceImpl implements EmployeeBusinessService {

    private final EmployeeFunctioanlService employeeService;

    private final EmailAvailabilityChecker emailChecker;
    private final PhoneNumberAvailabilityChecker phoneChecker;
    private final DepartmentRepetitionChecker departmentChecker;

    private final EmployeeMapper employeeMapper;

    @Override
    @Transactional
    public CommonResponse create(EmployeeRequest request) {
        phoneChecker.check(request.getPhone());

        if (request.getEmail() != null)
            emailChecker.check(request.getEmail());

        departmentChecker.check(request.getDepartment().getDepartmentName());

        CommonResponse response = new CommonResponse();
        Employee employee = employeeMapper.toEntity(request);
        employee.getAddress().setEmployee(employee);
        employee.getJobDetail().setEmployee(employee);
        employee = employeeService.create(employee);

        response.setItem(employeeMapper.toResponse(employee));
        return response;
    }

    @Override
    public CommonResponse findById(Long id) {
        CommonResponse response = new CommonResponse();
        response.setItem(employeeMapper.toResponse(employeeService.findActiveById(id)));
        return response;
    }

    @Override
    public CommonResponse findAll() {
        CommonResponse response = new CommonResponse();
        List<Employee> list = employeeService.findActiveEmployees();
        response.setItem(list.stream().map(employeeMapper::toResponse).collect(Collectors.toList()));
        return response;
    }

    @Override
    public CommonResponse update(Long id, EmployeeRequest request) {
        CommonResponse response = new CommonResponse();
        Employee employee = employeeMapper.toEntity(request);
        response.setItem(employeeMapper.toResponse(employeeService.update(id, employee)));
        return response;
    }

    @Override
    public CommonResponse delete(Long id) {
        CommonResponse response = new CommonResponse();
        response.setItem(employeeService.delete(id));
        return response;
    }
}
