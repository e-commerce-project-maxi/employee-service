package com.company.hremployeeservice.service.employee;

import com.company.hremployeeservice.domain.Employee;
import com.company.hremployeeservice.dto.request.EmployeeRequest;
import com.company.hremployeeservice.myException.common.CommonResponse;

import java.util.List;

public interface EmployeeFunctioanlService {

    Employee create(Employee employee);

    Employee findById(Long id);

    List<Employee> findAll();

    Employee update(Long id, Employee request);

    boolean delete(Long id);

    boolean checkExistsByPhone(String phone);

    boolean checkExistsByEmail(String email);

    Employee findActiveById(Long id);

    List<Employee> findActiveEmployees();
}
