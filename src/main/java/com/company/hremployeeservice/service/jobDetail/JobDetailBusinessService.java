package com.company.hremployeeservice.service.jobDetail;

import com.company.hremployeeservice.dto.request.JobDetailRequest;
import com.company.hremployeeservice.myException.common.CommonResponse;

public interface JobDetailBusinessService {

    CommonResponse create(JobDetailRequest request);

    CommonResponse findById(Long id);

    CommonResponse findAll();

    CommonResponse update(Long id, JobDetailRequest request);
}
